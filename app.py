from flask import Flask
from flask import request
import os 
import glob, os
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from os import environ
import json
import io
import pandas as pd
import xlrd


app = Flask(__name__)



@app.route("/")
def index():
    return "Python Microservice To fetch sentiment score"

@app.route("/sentimentanalysis")
def sentiment_anlysis():
    df = pd.ExcelFile('./ticket_data.xlsx').parse('tickets_resolved')
    var1=df['FeedBack']
    l=len(var1)
    print(l)
    data_sentiment =[{
        "Positive_Scores":[],
        "Negative_Scores":[],
        "Neutral_Scores":[],
        "Top_Five_Positives":[],
        "Top_Five_Negatives":[],
        "Overall_Sentiment_Score":0,
        "Overall_Positive_Score":0,
        "Overall_Negative_Score":0,
        "Overall_Neutral_Score":0
    }]
    score=0.0
    sent=0.0
    Positive_Score=0
    Negative_Score=0
    Neutral_Score=0

    analyzer = SentimentIntensityAnalyzer()
    for i in range(l):
        sentence=str(var1[i])
        pos = 0
        neg = 0
        neu = 0
        vs = analyzer.polarity_scores(sentence)
        sent += vs['compound']
        pos = pos + vs['pos']
        neg = neg + vs['neg']
        neu = neu + vs['neu']
        if neg > pos :
            feedback="negative"
            data_sentiment[0]["Negative_Scores"].append({"score":neg,"sentence":sentence})
            Negative_Score=Negative_Score+neg
        elif neg == pos :
            feedback = "neutral"
            data_sentiment[0]["Neutral_Scores"].append({"score":neu,"sentence":sentence})
            Neutral_Score=Neutral_Score+neu
        else:
            feedback ="positive"
            data_sentiment[0]["Positive_Scores"].append({"score":pos,"sentence":sentence})
            Positive_Score=Positive_Score+pos
    
    score =Positive_Score + Neutral_Score + Negative_Score

    Overall_Sentiment_Score= (sent / l ) * 100
    data_sentiment[0]["Overall_Sentiment_Score"] = round(Overall_Sentiment_Score, 2)

    Overall_Positive_Score = ( Positive_Score / score ) * 100
    data_sentiment[0]["Overall_Positive_Score"]=round(Overall_Positive_Score, 2)

    Overall_Negative_Score = ( Negative_Score / score ) * 100
    data_sentiment[0]["Overall_Negative_Score"]=round(Overall_Negative_Score, 2)

    Overall_Neutral_Score = ( Neutral_Score / score ) * 100
    data_sentiment[0]["Overall_Neutral_Score"]=round(Overall_Neutral_Score, 2)

    Positive_length = len(data_sentiment[0]["Positive_Scores"])
    Negative_length = len(data_sentiment[0]["Negative_Scores"])

    if Positive_length < 5 :
        data_sentiment[0]["Top_Five_Positives"].append((sorted(data_sentiment[0]["Positive_Scores"], key=repr, reverse=True))[:Positive_length] )  
    else:
        data_sentiment[0]["Top_Five_Positives"].append((sorted(data_sentiment[0]["Positive_Scores"], key=repr, reverse=True))[:-(Positive_length - 5)])
        
    if Negative_length < 5 :
        data_sentiment[0]["Top_Five_Negatives"].append((sorted(data_sentiment[0]["Negative_Scores"], key=repr, reverse=True))[:Negative_length])
    else:
        data_sentiment[0]["Top_Five_Negatives"].append((sorted(data_sentiment[0]["Negative_Scores"], key=repr, reverse=True))[:-(Negative_length - 5)])

        
    data_sentiment=json.dumps(data_sentiment ,indent=4, sort_keys=True)
    print(data_sentiment)
    return data_sentiment
    
if __name__ == "__main__":
    port = int(os.environ.get("PORT", 8080))
    app.run(host='0.0.0.0', port=port)